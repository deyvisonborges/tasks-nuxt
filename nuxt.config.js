module.exports = {
  css: [ 'assets/bootstrap.min.css',
  ],
  generate: {
    routes: [
      '/users/1',
      '/users/2',
      '/users/3'
    ]
  },
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content:'width=device-width, initial-scale=1' }
    ],
    link: [{
      rel: 'stylesheet',
      href: 'https://font.com',
    }]
  },
  transition: {
    name: 'page',
    mode: 'out-in'
  },
  // plugins: ['~/plugins/vue-notifications']
}
